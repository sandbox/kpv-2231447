<?php

/**
 * Theme csval settings rules lists.
 */
function theme_csval_rules_list($variables) {
  $type = $variables['type'];
  $items = $variables['items'];

  switch ($type) {
  case 'generic' :
    $header = array(t('Name'), t('form_id pattern'), t('Rules'), array('data' => t('Operations'), 'colspan' => 3));
    $rows = array();
    foreach ($items as $item) {
      $item = (array) $item;
      $name = l($item['name'], "admin/config/system/csval/edit/generic/{$item['rule_id']}");
      $edit_link = l(t('edit'), "admin/config/system/csval/edit/generic/{$item['rule_id']}");
      $delete_link = l(t('delete'), "admin/config/system/csval/delete/generic/{$item['rule_id']}");
      $manage_link = l($item['active'] ? t('disable') : t('enable'), "admin/config/system/csval/manage/generic/{$item['rule_id']}/" . ($item['active'] ? 'disable' : 'enable'));
      $row = array($name, $item['form_id_pattern'], str_replace("\n", '<br />', $item['rules']), $edit_link, $delete_link, $manage_link);
      $rows[] = $row;
    }
    $output = theme('table', array('header' => $header, 'rows' => $rows, 'caption' => '<h1>' . t('Generic rules') . '</h1>'));
    break;
  case 'field' :
    $header = array(t('Name'), t('Entity type'), t('Bundle'), t('Fields list'), array('data' => t('Operations'), 'colspan' => 3));
    $rows = array();
    foreach ($items as $item) {
      $item = (array) $item;
      $name = l($item['name'], "admin/config/system/csval/edit/field/{$item['rule_id']}");
      $edit_link = l(t('edit'), "admin/config/system/csval/edit/field/{$item['rule_id']}");
      $delete_link = l(t('delete'), "admin/config/system/csval/delete/field/{$item['rule_id']}");
      $manage_link = l($item['active'] ? t('disable') : t('enable'), "admin/config/system/csval/manage/field/{$item['rule_id']}/" . ($item['active'] ? 'disable' : 'enable'));
      $row = array($name, $item['entity_type'], $item['entity_bundle'], str_replace("\n", '<br />', $item['fields_list']), $edit_link, $delete_link, $manage_link);
      $rows[] = $row;
    }
    $output = theme('table', array('header' => $header, 'rows' => $rows, 'caption' => '<h1>' . t('Field rules') . '</h1>'));
    break;
  }

  return $output;
}

