<?php

$plugin = array(
  'field_type' => 'text',
  'widget_type' => 'text_textfield',
  'preprocessor' => 'csval_preprocessor__text__text_textfield',
  'after_build' => 'csval_preprocessor__text__text_textfield__after_build',
);


function csval_preprocessor__text__text_textfield(&$element, &$form_state, $context) {
}


function csval_preprocessor__text__text_textfield__after_build($element, $form_state) {
  $rule_plugins = $element['#csval']['#rules']['#plugins'];
  $js_rules = csval_element_rule_plugins_process($element, $form_state, $rule_plugins);
  dsm($js_rules);

  $field_name = $element['#field_name'];
  $lang = $element['#language'];
  $instance = $form_state['field'][$field_name][$lang]['instance'];
  $field = $form_state['field'][$field_name][$lang];

  // attach javascript file
  $element['#attached']['js'][] = drupal_get_path('module', 'csval_api') . '/js/csval.api.js';
  $element['#attached']['js'][] = drupal_get_path('module', 'csval_api') . '/js/csval.methods.js';

  $settings = array();
  $rules = array();
  foreach ($js_rules as $k => $js_rule) {
    $id = "#{$form_state['complete form']['#id']}";
    $name = $js_rule['name'];
    $settings[$id][$name]['rules'][key($js_rule['rule'])] = array(
      'info' => reset($js_rule['rule']),
      'message' => $js_rule['message'],
      'event' => 'focusout',
    );
  }
  $settings = array('csval' => $settings);

  $element['#attached']['js'][] = array('data' => $settings, 'type' => 'setting');

  return $element;
}

