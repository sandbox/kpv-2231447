<?php

$plugin = array(
  'element_type' => 'select',
  'preprocessor' => 'csval_preprocessor__input__select',
  'after_build' => 'csval_preprocessor__input__select__after_build',
);


/**
 * Element preprocessor
 *
 * @todo
 */
function csval_preprocessor__input__select(&$element, &$form_state) {
}


/**
 * plugin #after_build
 *
 * @todo
 */
function csval_preprocessor__input__select__after_build($element, $form_state) {
  // @todo: collect rules for the element
  $rule_plugins = $element['#csval']['#rules']['#plugins'];
  $js_rules = csval_element_rule_plugins_process($element, $form_state, $rule_plugins);
  dsm($js_rules);

  // attach javascript file
  $element['#attached']['js'][] = drupal_get_path('module', 'csval_api') . '/js/csval.api.js';
  $element['#attached']['js'][] = drupal_get_path('module', 'csval_api') . '/js/csval.methods.js';


  // attach javascript settings
  $settings = array();
  $rules = array();
  foreach ($js_rules as $k => $js_rule) {
    $id = "#{$form_state['complete form']['#id']}";
    $name = $element['#name'];
    $settings[$id][$name]['rules'][key($js_rule['rule'])] = array(
      'info' => reset($js_rule['rule']),
      'message' => $js_rule['message'],
      'event' => 'focusout',
    );
  }
  $settings = array('csval' => $settings);
  $element['#attached']['js'][] = array('data' => $settings, 'type' => 'setting');
  return $element;
}
