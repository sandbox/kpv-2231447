<?php

// This plugin builds/completes validation scheme for current element/form based on validation rules.

$plugin = array(
  'element_type' => 'textfield',
  'preprocessor' => 'csval_preprocessor__input__textfield',
  'after_build' => 'csval_preprocessor__input__textfield__after_build',
);

/**
 * Element preprocessor
 *
 * @todo
 * Seems that in most cases preprocessor won't be needed.
 */
function csval_preprocessor__input__textfield(&$element, &$form_state) {
  // Here order of rule_plugins could be changed.
  // Or even plugins list could be altered, i.e. added other plugins.
}


/**
 * Element #after_build
 *
 * @todo
 * Seems that in most cases preprocessor won't be needed.
 * Maybe csval_element_rule_plugins_process() should be called from module file but not plugin.
 */
function csval_preprocessor__input__textfield__after_build($element, $form_state) {
  // @todo: collect rules for the element
  $rule_plugins = $element['#csval']['#rules']['#plugins'];
  $js_rules = csval_element_rule_plugins_process($element, $form_state, $rule_plugins);
  dsm($js_rules);

  // attach javascript file
  $element['#attached']['js'][] = drupal_get_path('module', 'csval_api') . '/js/csval.api.js';
  $element['#attached']['js'][] = drupal_get_path('module', 'csval_api') . '/js/csval.methods.js';


  // attach javascript settings
  // $id = $element['#id'];
  // currently required keys : form_id, name, rule, message
  /*
  $rules = array(
    array(
      'name' => $element['#name'],
      'rule' => array('required' => true),
      'message' => t('Message text here'), // @todo: Use csval_t() instead of t() to allow message altering.
      'form_id' => "#{$form_state['complete form']['#id']}",
      'id' => "#{$id}",
      'input_type' => 'textfield', // maybe this could be defined according to #id
      'event' => 'focusout',
      'conditions' => array(),
      'weight' => 0,  // rule weight in the set defines order in with the are applied
      'rule_uid' => 'some-unique-id', // unique id of the rule. could be used for form validation state on client side.
      'action' => 'some_action',  // the way message is set or maybe some other action triggered when element validation fails
    ),
  );
  */
  $settings = array();
  $rules = array();
  foreach ($js_rules as $k => $js_rule) {
    $id = "#{$form_state['complete form']['#id']}";
    $name = $element['#name'];
    $settings[$id][$name]['rules'][key($js_rule['rule'])] = array(
      'info' => reset($js_rule['rule']),
      'message' => $js_rule['message'],
      'event' => 'focusout',
    );
  }
  $settings = array('csval' => $settings);
  $element['#attached']['js'][] = array('data' => $settings, 'type' => 'setting');
  return $element;
}
