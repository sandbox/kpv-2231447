<?php

$plugin = array(
  'element_type' => 'select',
  'rules_callback' => 'csval_rule__input__select__rules_callback',
);

/**
 * plugin rules callback
 *
 * @todo
 */
function csval_rule__input__select__rules_callback($element, $form_state) {
  $plugin_js_rules = array();
  if (!empty($element['#required'])) {
    $plugin_js_rule['rule'] = array('required' => true);
    $plugin_js_rule['message'] = t('This message refers to required select generic element.');  // @todo: use csval_t()
    $plugin_js_rules[] = $plugin_js_rule;
  }
  return $plugin_js_rules;
}
