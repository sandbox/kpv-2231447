<?php

// @todo: Rename plugin file, add suffix "__common".
// Also this should be somehow taken into consideration in rules_callback name
// to avoid use of the same function names in multiple plugins.

// Current plugin checks for most common textfield properties, listed in Form API Reference, i.e. #required, #maxlength.

// This plugin adds js rules for current element depending on its properties.
// These rules are not automatically attached but processed by validator plugin
// that prepares validation scheme for the element.

// The purpose of rules plugins is to supply validator plugins with consistent info about rules.
// So set of info elements returned by rules_callback can differ among different kind of elements.

$plugin = array(
  'element_type' => 'textfield',
  // 'after_build' => 'csval_rule__input__textfield__after_build',
  'rules_callback' => 'csval_rule__input__textfield__rules_callback',
);


/**
 * #after_build
 *
 * @todo
 */
/*
function csval_rule__input__textfield__after_build($element, &$form_state) {
  return $element;
}
*/


/**
 * Rules callback
 *
 * Attaches rules for further processing in validator plugin.
 * @todo
 * Maybe pass list of rules, already prepared by other plugins.
 */
// function csval_rule__input__textfield__rules_callback(&$element, $form_id, $form_state, $form) {
function csval_rule__input__textfield__rules_callback($element, $form_state) {
  $plugin_js_rules = array();
  if (!empty($element['#required'])) {
    $plugin_js_rule['rule'] = array('required' => true);
    $plugin_js_rule['message'] = t('This message refers to required field.');  // @todo: use csval_t()
    $plugin_js_rules[] = $plugin_js_rule;
  }
  if (!empty($element['#maxlength'])) {
    // $plugin_js_rule['rule'] = array('maxlength' => $element['#maxlength']);
    $plugin_js_rule['rule'] = array('maxlength' => 50);
    $plugin_js_rule['message'] = t('This message refers to maxlength field.');  // @todo: use csval_t()
    $plugin_js_rules[] = $plugin_js_rule;
  }
  return $plugin_js_rules;
}
