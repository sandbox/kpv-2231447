<?php

$plugin = array(
  'field_type' => 'text',
  'widget_type' => 'text_textfield',
  'rules_callback' => 'csval_preprocessor__text__text_textfield__rules_callback',
);


function csval_preprocessor__text__text_textfield__rules_callback($element, $form_state) {
  $field_name = $element['#field_name'];
  $lang = $element['#language'];
  $field_state_info = $form_state['field'][$field_name][$lang];
  $instance = $field_state_info['instance'];
  $field = $field_state_info['field'];

  //dsm($instance);
  //dsm($field);
  //dsm($element);
  //dsm($form_state);

  $plugin_js_rules = array();
  if ($instance['required'] && $field['cardinality'] == 1) {
    // @todo
    $plugin_js_rule['rule'] = array('required' => true);
    $plugin_js_rule['message'] = t('This message refers to required field.');  // @todo: use csval_t()
    $plugin_js_rule['name'] = $element['value']['#name'];
    $plugin_js_rules[] = $plugin_js_rule;
  }

  return $plugin_js_rules;
}
