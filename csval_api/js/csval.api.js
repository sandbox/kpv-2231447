(function ($) {

  // Generic validator object
  // @todo: use prototype
  var csvalValidator = {
    forms: [],
    // Initialize js settings
    // @todo
    initJs: function(jsSettings){
      //console.log(jsSettings);
      for( var form_id in jsSettings ) {
        // @todo check if form with given id exists.
        var csvalFormSettings = { id : form_id, form : $(form_id).first(), elements : {}, elementsState : {} };
        for( var name in jsSettings[form_id] ) {
          // @todo check if element with given name exists.
          var element = $(form_id+" :input[name='"+name+"']").first();
          this.elementJsEvents(element, jsSettings[form_id][name].rules);
          csvalFormSettings.elements[name] = element;
        }
        this.forms.push(csvalFormSettings);
      }
      //console.log(this);
    },

    // Initialize ajax settings
    // @todo
    // select only ajax valdation settings
    initAjax: function(ajaxSettings) {
      //console.log(ajaxSettings);
    },

    // get list of triggering events and bind them to the element
    elementJsEvents: function(element, rules) {
      element.csvalRules = rules;
      element.csvalEvents = {};
      for (var rule in rules) {
        var ruleEvent = rules[rule].event;
        if (!( ruleEvent in element.csvalEvents )) {element.csvalEvents[ruleEvent] = [] }
        element.csvalEvents[ruleEvent].push(rule);
      }
      // bind collected events to the element:w
      for (var elementEvent in element.csvalEvents) {
        element.bind(elementEvent, function(){
          // @todo: pass valid info
          $(this).trigger('genericTriggerJs', { triggeringEvent : elementEvent } );
        });
      }
    },

    // validate element against given rule
    validateElement: function(element, ruleName) {
      // @todo: file with validator methods must be included before current one.
      var validator = new $.csvalValidator;
      var value = $(element).val();
      return validator.methods[ruleName](value);
    },

    // decide which function handles error messages and apply it.
    placeError: function(passed, element, ruleName) {
      var op = !passed ? 'set' : 'unset';
      this.defaultErrorPlacement(op, element, ruleName);
    },

    // default error messages handling function
    defaultErrorPlacement: function(op, element, ruleName) {
      var name = $(element).attr('name');
      if (op == 'set') {
        var message = element.csvalRules[ruleName].message;
        // remove old error messages if any
        this.defaultErrorPlacement('unset', element, ruleName);
        // set new error message
        var error_id = $(element).attr('id') + '-cserror';
        var html = "<div id='"+error_id+"'>" + message + "</div>";
        $(element).after(html);
        var err_new = $('#'+error_id);
        // @todo: define form. currently, this will only work for one form.
        if (!(name in this.forms[0].elementsState)) {
          this.forms[0].elementsState[name] = {err_obj : {} };
        }
        this.forms[0].elementsState[name].err_obj = err_new;
      }
      else if (op == 'unset') {
        if (name in this.forms[0].elementsState) {
          // @todo: check if not undefined
          var err_old = this.forms[0].elementsState[name].err_obj;
          $(err_old).remove();
          console.log(this);
        }
      }
    }
  };

  $(document).on('genericTriggerJs', function(e, eventInfo) {
    var triggeringEvent = eventInfo.triggeringEvent;
    // @todo: csvalEvents and csvalRules should be attached to element itself for easier access.
    //var element = e.target;

    var name = $(e.target).attr('name');
    var form_id = $(e.target).closest('form').attr('id');
    for (var i in csvalValidator.forms) {
      if ("#"+form_id == csvalValidator.forms[i].id) {
        var form = csvalValidator.forms[i].form;
        var element = csvalValidator.forms[i].elements[name];
        break;
      }
    }

    if (typeof element != undefined) {
      var csvalEvents = element.csvalEvents;
      var csvalRules = element.csvalRules;
      for (var i in csvalEvents[triggeringEvent]) {
        var ruleName = csvalEvents[triggeringEvent][i];
        var passed = csvalValidator.validateElement(element, ruleName);
        if (!passed) {
          // @todo: this should be implemented as element method.
          csvalValidator.placeError(passed, element, ruleName);
          break;
        }
        else {
          // remove error
          csvalValidator.placeError(passed, element, ruleName);
        }
      }
    }
  });
  

  Drupal.behaviors.csval = {
    attach: function (context, settings) {
      csvalValidator.initJs(settings.csval);
      csvalValidator.initAjax(settings.ajax);
    }
  };
})(jQuery);

